//package com.xzjie.cms.convert;
//
//import org.mapstruct.Mapper;
//import org.mapstruct.ReportingPolicy;
//
//import com.xzjie.cms.dto.MenuResponse;
//import com.xzjie.cms.model.Menu;
//
//@Mapper(componentModel = Converter.componentModel, uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
//public interface MenuConverter extends Converter<MenuResponse, Menu> {
//}
