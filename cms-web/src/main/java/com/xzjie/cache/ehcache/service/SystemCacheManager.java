package com.xzjie.cache.ehcache.service;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

public class SystemCacheManager {

	private static Cache cache;

	public SystemCacheManager(CacheManager cacheManager) {
		SystemCacheManager.cache = cacheManager.getCache("system-cache");
	}

	public static Object get(String key) {
		try {
			return cache.get(key).get();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T get(String key, Class<T> cls) {
		try {
			return (T) cache.get(key).get();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void put(String key, Object value) {
		try {
			cache.put(key, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void evict(String key) {
		try {
			cache.evict(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void clear() {
		try {
			cache.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
